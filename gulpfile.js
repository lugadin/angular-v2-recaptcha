var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require("gulp-rename");
var header = require('gulp-header');
var d = new Date();
var headerComment = '/*Create on: ' + d + ' */\n';
gulp.task('js', function () {
    return gulp.src(['src/*.js'])
            .pipe(concat('angular-v2-recaptcha.js'))
            .pipe(header(headerComment))
            .pipe(gulp.dest('release'))
            .pipe(rename({suffix: ".min"}))
            .pipe(uglify())
            .pipe(header(headerComment))
            .pipe(gulp.dest('release'))
            ;
});

