/*global angular, Recaptcha */
(function (ng) {
    'use strict';

    function throwNoKeyException() {
        throw new Error('You need to set the "key" attribute to your public reCaptcha key. If you don\'t have a key, please get one from https://www.google.com/recaptcha/admin/create');
    }
    ng.module('reCaptcha-v2', [])
            .provider('reCaptcha', [function () {
                    var siteKey, theme, size;
                    this.setSiteKey = function (_siteKey) {
                        siteKey = _siteKey;
                    };
                    this.setSize = function (_size) {
                        size = _size;
                    };
                    this.setTheme = function (_theme) {
                        theme = _theme;
                    };
                    this.$get = ['$document', function ($document) {
                            var s = $document[0].createElement('script');
                            var src = 'https://www.google.com/recaptcha/api.js?onload=recaptchaLoaded&render=explicit';
                            s.type = 'application/javascript';
                            s.src = src;
                            $document[0].body.appendChild(s);
                            return {
                                theme: theme,
                                siteKey: siteKey,
                                size: size
                            };
                        }];
                }])
            .service('recaptchaService', ['$window', '$q', function ($window, $q) {

                    var deferred = $q.defer(), promise = deferred.promise, recaptcha;

                    $window.recaptchaLoadedCallback = $window.recaptchaLoadedCallback || [];

                    var callback = function () {
                        recaptcha = $window.grecaptcha;
                        deferred.resolve(recaptcha);
                    };

                    $window.recaptchaLoadedCallback.push(callback);

                    $window.recaptchaLoaded = function () {
                        $window.recaptchaLoadedCallback.forEach(function (callback) {
                            callback();
                        });
                    };


                    function getRecaptcha() {
                        if (!!recaptcha) {
                            return $q.when(recaptcha);
                        }
                        return promise;
                    }
                    function validateRecaptchaInstance() {
                        if (!recaptcha) {
                            throw new Error('reCaptcha has not been loaded yet.');
                        }
                    }
                    if (ng.isDefined($window.grecaptcha)) {
                        callback();
                    }

                    return {
                        create: function (elm, key, fn, conf) {
                            conf.callback = fn;
                            conf.sitekey = key;
                            return getRecaptcha().then(function (recaptcha) {
                                return recaptcha.render(elm, conf);
                            });
                        },
                        reload: function (widgetId) {
                            validateRecaptchaInstance();
                            recaptcha.reset(widgetId);
                        },
                        getResponse: function (widgetId) {
                            validateRecaptchaInstance();
                            return recaptcha.getResponse(widgetId);
                        }
                    };

                }])
            .directive('recaptcha', ['$document', '$timeout', 'recaptchaService', 'reCaptcha', function ($document, $timeout, recaptchaService, reCaptcha) {
                    return {
                        restrict: 'A',
                        require: "?^^form",
                        scope: {
                            response: '=?ngModel',
                            key: '=',
                            stoken: '=?',
                            theme: '=?',
                            size: '=?',
                            type: '=?',
                            tabindex: '=?',
                            required: '=?',
                            onCreate: '&',
                            onSuccess: '&',
                            onExpire: '&'
                        },
                        link: function (scope, elm, attrs, ctrl) {
                            scope.widgetId = null;

                            if (ctrl && angular.isDefined(attrs.required)) {
                                scope.$watch('required', validate);

                            }
                            var removeCreationListener = scope.$watch('key', function (key) {
                                key = key || reCaptcha.siteKey;
                                if (!key) {
                                    return;
                                }
                                if (key.length !== 40) {
                                    throwNoKeyException();
                                }
                                var callback = function (gRecaptchaResponse) {
                                    $timeout(function () {
                                        scope.response = gRecaptchaResponse;
                                        validate();
                                        scope.onSuccess({response: gRecaptchaResponse, widgetId: scope.widgetId});
                                    });
                                };

                                recaptchaService.create(elm[0], key, callback, {
                                    stoken: scope.stoken || attrs.stoken || null,
                                    theme: scope.theme || attrs.theme || reCaptcha.theme || null,
                                    type: scope.type || attrs.type || null,
                                    tabindex: scope.tabindex || attrs.tabindex || null,
                                    size: scope.size || attrs.size || reCaptcha.size || null,
                                    'expired-callback': expired

                                }).then(function (widgetId) {
                                    validate();
                                    scope.widgetId = widgetId;
                                    scope.onCreate({widgetId: widgetId});
                                    scope.$on('$destroy', destroy);
                                });
                                removeCreationListener();
                            });

                            function destroy() {
                                if (ctrl) {
                                    ctrl.$setValidity('recaptcha', null);
                                }
                                cleanup();
                            }

                            function expired() {
                                scope.response = "";
                                validate();
                                scope.onExpire({widgetId: scope.widgetId});

                                scope.$apply();
                            }

                            function validate() {
                                if (ctrl) {
                                    ctrl.$setValidity('recaptcha', scope.required === false ? null : Boolean(scope.response));
                                }
                            }

                            function cleanup() {
                                angular.element($document[0].querySelectorAll('.pls-container')).parent().remove();
                            }
                        }
                    };
                }]);
}(angular));